const storageKey = "myTerms";
const limit = 100;
let options = {};

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function execute() {
  const nodeIterator = document.createNodeIterator(
    // Node to use as root
    document.body,

    // Only consider nodes that are text nodes (nodeType 3)
    NodeFilter.SHOW_TEXT,

    // Object containing the function to use for the acceptNode method
    // of the NodeFilter
    {
      acceptNode: function (node) {
        // Logic to determine whether to accept, reject or skip node
        // In this case, only accept nodes that have content
        // other than whitespace
        if (/[A-Za-z0-9\s-_]/.test(node.data)) {
          return document.activeElement !== node && NodeFilter.FILTER_ACCEPT;
        }
      },
    },
    false
  );

  const textnodes = [];

  while ((currentNode = nodeIterator.nextNode())) {
    textnodes.push(currentNode);
  }

  if (textnodes.length > 0) {
    processText(textnodes);
  }
}

function processText(textnodes) {
  chrome.storage.sync.get(storageKey, options => {
    const mt = options.myTerms;
    if (mt.words !== undefined && mt.words.length > 0) {
      for (const word of mt.words) {
        replaceWords(textnodes, word);
      }
    }
  });
}

function replaceWords(textnodes, word) {
  for (let i = 0, len = textnodes.length; i < len; i++) {
    let _nv = textnodes[i].nodeValue;
    let opts = "g";

    if (word.ignoreCase === true) {
      opts += "i";
    }

    _nv = _nv.replace(new RegExp("\\b" + word.find + "\\b", opts), word.replace);

    textnodes[i].nodeValue = _nv;
  }
}

function process() {
  const xhr = new XMLHttpRequest();
  xhr.addEventListener("loadend", event => {
    execute();
  });

  window.addEventListener("load", event => {
    execute();
  });

  document.addEventListener("DOMContentLoaded", event => {
    execute();
  });
}

chrome.storage.sync.get(storageKey, options => {
  let doProcess = true;
  const mt = options.myTerms;

  if (mt.domains !== undefined && mt.domains.length > 0) {
    const currentHost = window.location.hostname;
    for (let domain of mt.domains) {
      const url = new URL(domain);
      const hostname = url.hostname;
      if (hostname === currentHost) {
        doProcess = false;
        break;
      }
    }

    if (mt.words === undefined || mt.words.length === 0) {
      doProcess = false;
    }
  }

  if (doProcess) {
    process();
  }
});
